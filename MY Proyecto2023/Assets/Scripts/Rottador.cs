using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rottador : MonoBehaviour
{
    public Transform cubex;

    void Update()
    {
        Vector3 targetOrientation = cubex.position - transform.position;
        Debug.DrawRay(transform.position, targetOrientation, Color.blue);

        Quaternion targetOrientationQuaternion = Quaternion.LookRotation(targetOrientation);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetOrientationQuaternion, Time.deltaTime);
    }
}
