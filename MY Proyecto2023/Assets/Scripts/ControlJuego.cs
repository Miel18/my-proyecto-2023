using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ControlJuego : MonoBehaviour
{
    public GameObject Personaje;
    public GameObject Enemy;
    public ControlEnemy ControlEnemy;
    private List<GameObject> listaEnemys = new List<GameObject>();
    float tiempoRestante;

    void Start()
    {
        ComenzarJuego();
    }
    void Update()
    {
        if (tiempoRestante == 0)
        {
            ComenzarJuego();
        }

    }
    void ComenzarJuego()
    {
        Personaje.transform.position = new Vector3(0f, 0f, 0f);
        foreach (GameObject item in listaEnemys)
        {
            Destroy(item);
        }

        listaEnemys.Add(Instantiate(Enemy, new Vector3(6.64f, 1f, -10.06f), Quaternion.identity));
        listaEnemys.Add(Instantiate(Enemy, new Vector3(3.83f, 1f, -19.45f), Quaternion.identity));
        listaEnemys.Add(Instantiate(Enemy, new Vector3(3.24f, 1f, -12.52f), Quaternion.identity));
        StartCoroutine(ComenzarCronometro(25));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 25)
    {
        tiempoRestante = valorCronometro;

        while (tiempoRestante > 0)
        {
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }

}