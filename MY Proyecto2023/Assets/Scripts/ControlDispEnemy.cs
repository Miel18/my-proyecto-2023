using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDispEnemy : MonoBehaviour
{
    //[SerializeField]
    public GameObject Proyectil;
    //[SerializeField]
    public float timer = 2f;
    //private float timerConta = 0;

    public int disparos;
    //[SerializeField]
    public int maxdDisparos = 20;
    void Start()
    {
        StartCoroutine(FuegoBull());
    }


    IEnumerator FuegoBull()
    {
        Debug.Log("iniciado");
        for(int i = 0; i <maxdDisparos; i++)
        {
            disparos++;
            Instantiate(Proyectil, transform.position, transform.rotation);
            yield return new WaitForSeconds(timer);
            Destroy(Proyectil, 3);
        }
        Debug.Log("Fin corotine");
    }
}
