using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlPersonaje : MonoBehaviour
{

    public float rapidezDesplazamiento = 1.5f;
    public LayerMask capaPiso;  //capa de mascara
    public float magnitudSalto; //magnitud de fuerza
    public CapsuleCollider col; //colición
    private Rigidbody rb;
    float xInicial, yInicial, zInicial; //x,y,z posiciones
    float xT, yT, zT;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
    public Camera camaraPrimeraPersona;
    public GameObject Proyectil;
    private int hpPerson; 



    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        xInicial = transform.position.x; //La posicion del valor de x
        yInicial = transform.position.y; //La posicion del valor de y   (osea desde que comenzó)
        zInicial = transform.position.z; //La posicion del valor de z
        cont = 0;
        textoGanaste.text = "";
        setearTextos();
        hpPerson = 100;
    }


    public void recibirDañoPerson()
    {
        hpPerson = hpPerson - 25;
        if (hpPerson <= 0)
        {
            this.gameOver();
        }

    }

    private void gameOver()
    {
        textoGanaste.text = "Perdiste!";

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name.Substring(0, 5) == "Enemy")
        {
            recibirDañoPerson();
        }
    }
    

    private void setearTextos()
    { 
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString(); 
        if (cont >= 2) 
        { 
            textoGanaste.text = "Ganaste!"; 
        } 
    }

    void Update() 
    { 
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento; 
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento; 

        movimientoAdelanteAtras *= Time.deltaTime; 
        movimientoCostados *= Time.deltaTime; 
        
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras); 
        
        if (Input.GetKeyDown("escape")) 
        { 
            Cursor.lockState = CursorLockMode.None; 
        }

        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())    //comprobacion de si esta en piso al presionar tecla "espacio"
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse); //agrega la fuerza de salto
        }

        xT += 0.03f * Time.deltaTime;
        yT += 0.03f * Time.deltaTime;
        zT += 0.03f * Time.deltaTime;



        if (Input.GetMouseButtonDown(0)) 
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); 
            GameObject pro; 
            pro = Instantiate(Proyectil, ray.origin, transform.rotation); 
            Rigidbody rb = pro.GetComponent<Rigidbody>(); 
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse); 
            Destroy(pro, 5); 
        }
    }

    private bool EstaEnPiso()//true/false
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
        //chequeo de la colicion cuandon la capsula toca el piso
    }

    public void Respawn() //Lo posiciona en x,y,z de inicio
    {
        transform.position = new Vector3(xInicial, yInicial, zInicial);
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("coleccionable") == true) 
        { 
            cont = cont + 1;
            setearTextos(); 
            other.gameObject.SetActive(false);
            transform.localScale = new Vector3(xT, yT, zT);
            rapidezDesplazamiento = rapidezDesplazamiento + 1;
        }
    }

}