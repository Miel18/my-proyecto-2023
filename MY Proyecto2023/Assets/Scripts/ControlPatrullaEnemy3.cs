using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class ControlPatrullaEnemy3 : MonoBehaviour
{
    public NavMeshAgent _enemy3;

    public Transform personaje;
    public LayerMask capaPiso, capaPisoPerson;
    public float hp;

    //patrulla
    public Vector3 puntoCaminar;
    bool PuntosPasea;
    public float rangoCaminata;

    //ataque
    public float tiempoAtack;
    bool Atacar;
    public GameObject proyectil;

    //
    public float divicionRango, rangoAtack;
    public bool jugadorEnRango, jagardorAtacado;

    private void Awake()
    {
        personaje = GameObject.Find("Personaje").transform;
        _enemy3 = GetComponent<NavMeshAgent>();
    }



    void Start()
    {

    }

    void Update()
    {
        //verifica al jugador si esta a vista o en rango ataque
        jugadorEnRango = Physics.CheckSphere(transform.position, divicionRango, capaPisoPerson);
        jagardorAtacado = Physics.CheckSphere(transform.position, rangoAtack, capaPisoPerson);
        if (!jugadorEnRango && !jagardorAtacado) Patrullar();
        if (jugadorEnRango && !jagardorAtacado) Perseguir();
        if (jugadorEnRango && jagardorAtacado) AtacarPerson();
    }

    private void Patrullar()
    {
        if (!PuntosPasea)
        {
            Recorrido();
        }


        if (PuntosPasea)
        {

            _enemy3.SetDestination(puntoCaminar);

            Vector3 distanciaCaminar = transform.position - puntoCaminar;

            if (distanciaCaminar.magnitude < 1f)
            {
                PuntosPasea = true;
            }

        }
    }

    private void Recorrido()
    {
        float randomZ = Random.Range(-rangoCaminata, rangoCaminata);
        float randomX = Random.Range(-rangoCaminata, rangoCaminata);

        puntoCaminar = new Vector3(transform.position.x + randomX, transform.position.z + randomZ);
        if (Physics.Raycast(puntoCaminar, -transform.up, 2f, capaPiso))
        {
            PuntosPasea = true;
        }
    }
    private void Perseguir()
    {
        _enemy3.SetDestination(personaje.position);
    }

    private void AtacarPerson()
    {
        _enemy3.SetDestination(transform.position);
        transform.LookAt(personaje);
        if (!Atacar)
        {
            Rigidbody rb = Instantiate(proyectil, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            rb.AddForce(transform.up * 8f, ForceMode.Impulse);

            Atacar = true;
            Invoke(nameof(ResetAtacar), tiempoAtack);
        }

    } 

    private void ResetAtacar()
    {
        Atacar = false;
    }

    public void da�o(int Da�o)
    {
        hp -= Da�o;
        if (hp <= 0) Invoke(nameof(DestroyE3), 0.5f);
    }
    private void DestroyE3()
    {
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, rangoAtack);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, divicionRango);
    }


}
